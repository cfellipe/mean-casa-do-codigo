module.exports = function(app) {

	var controllerContato = app.controllers.contato;

	app.route('/contatos')
		.get(controllerContato.listaContatos);
		
	app.route('/contatos/:id')
	   .get(controllerContato.obtemContato);
	
};