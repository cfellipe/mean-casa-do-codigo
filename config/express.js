var load = require('express-load'),
    express = require('express'),
    bodyParser = require('body-parser')
    home = require('../app/routes/home');


module.exports = function(){
	//Utilizando o Express
    var app = express();

    //Variaveis de Ambiente
    app.set('port', 3000);

    //Middlewares
    app.use(express.static('./public'));
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(require('method-override')());

    //Template Engines
    app.set('view engine', 'ejs');
    app.set('views', './app/views');

    //Rotas
    load('models',{cwd:'app'})
        .then('controllers')
        .then('routes')
        .into(app);

    //Retornando a aplicação
    return app;
}